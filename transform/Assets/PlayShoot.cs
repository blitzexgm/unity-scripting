﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayShoot : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit rayHit;
        Ray rayTest = new Ray(transform.position, transform.forward);

        if (Physics.Raycast(rayTest, out rayHit, 1000))
        {
            if (rayHit.collider.gameObject.tag == "Sphere")
            {
                Destroy(rayHit.collider.gameObject);
            }
        }
    }
}
