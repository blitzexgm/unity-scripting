﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transformacion : MonoBehaviour
{
    // Capsule speed when it´s moving
    private float speed = 5f;

    //Use this for initialization
    void Start()
    {
        //Move five unity space units per second towards front direction (only once at initialization)
        transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        Debug.Log("Moving From Start Event");
    }

    // Update is called once per frame
    void Update()
    {
        //Move five Unity space units per second towards right direction (executed every from)
        transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        Debug.Log("Moving From Update Event");
    }
}