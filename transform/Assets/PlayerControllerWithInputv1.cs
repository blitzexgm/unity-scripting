﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv1 : MonoBehaviour {

    public float speed = 5;

    void Update()
    {
        // Z axis movement
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Debug.Log("Key UpArrow pressed");
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            Debug.Log("Key DownArrow pressed");
            transform.Translate(Vector3.back * Time.deltaTime * speed);
        }

        // X axis movement
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Debug.Log("Key RightArrow pressed");
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            Debug.Log("Key LeftArrow pressed");
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
    }
}