﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class escala : MonoBehaviour
{
    // Capsule scaleUnits when it´s moving
    public float scaleUnits = 5f;

    //Use this for initialization
    void Start()
    {
        //Add scaleUnits per second to transform localscale x axis (only once at initialization)
        transform.localScale = new Vector3(transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1);
        Debug.Log("Scaling From Start Event");
    }

    // Update is called once per frame
    void Update()
    {
        //Add scaleUnits per second to transform localscale x axis (executed every frame)
        transform.localScale = new Vector3(transform.localScale.x + scaleUnits * Time.deltaTime, 1, 1);
        Debug.Log("Scaling From Update Event");
    }
}