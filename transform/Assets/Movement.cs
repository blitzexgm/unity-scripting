﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speedH = 2.0f;
    public float speedV = 2.0f;
    private float yaw = 0.0f;
    private float pitch = 0.0f;
    public float speed = 1.0f; //how fast the object should rotate
    private float hInput, vInput;
    public float step;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        Rigidbody rb = GetComponent<Rigidbody>();
        hInput = Input.GetAxisRaw("Horizontal"); // Left = -1, Center = 0, Right = 1 (Discrete)
        vInput = Input.GetAxisRaw("Vertical"); // Down = -1, Center = 0, Up = 1 (Discrete)
        // Vertical Movement (Z axis)
        if (vInput != 0)
        {
            rb.velocity = new Vector3(vInput, 0, 0) *14f;
        }
        // Horizontal Movement (X axis)
        if (hInput != 0)
        {
            rb.velocity = new Vector3(0, 0, hInput)* 14f;
        }

    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Sphere")
        {
            Destroy(gameObject);
        }
    }
}