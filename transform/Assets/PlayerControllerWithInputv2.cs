﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv2 : MonoBehaviour {

    public float speed;
    public float runSpeed;
    private float hInput, vInput;
    public bool rotacion;
    public Color randomcolor;
    public bool coloraleatorio;
    Vector3 inicial;
    void Start()
    {
        inicial = transform.localScale;
    }
    void Update()
    {
        Renderer rend = GetComponent<Renderer>();
        //Get Input every frame
        hInput = Input.GetAxisRaw("Horizontal"); // Left = -1, Center = 0, Right = 1 (Discrete)
        vInput = Input.GetAxisRaw("Vertical"); // Down = -1, Center = 0, Up = 1 (Discrete)
        
        float step = speed;

        //Change speed if Run Button is pressed
        if (Input.GetButton("Run"))
        {
            step = runSpeed;
        }

        // Vertical Movement (Z axis)
        if (vInput != 0)
        {
            transform.Translate(vInput * Vector3.forward * Time.deltaTime * step);
        }

        // Horizontal Movement (X axis)
        if (hInput != 0)
        {
            if (rotacion == true)
            {
                transform.Rotate(hInput * Vector3.right * Time.deltaTime * step * 100);
            }
            else
            {
                transform.Translate(hInput * Vector3.right * Time.deltaTime * step);
            }
        }
        if (Input.GetButton("Color"))
        {
            randomcolor = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
            if (!coloraleatorio)
            {
                rend.material.SetColor("_Color", Color.red);
            }
            else
            {
                rend.material.SetColor("_Color", randomcolor);
            }
            
        }
        else
            rend.material.SetColor("_Color", Color.white);
        if (Input.GetButton("Escalar"))
        {
            transform.localScale += new Vector3(Random.Range(0, 2), Random.Range(0, 2), Random.Range(0, 2));
        } else {
            transform.localScale = inicial;
        }
    }
}
