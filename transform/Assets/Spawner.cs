﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject objectToSpawn;
    GameObject objAux;
    Renderer rend;
    float increment;
    [Range(0.5f, 3f)]
    public float limit;
    public Color randomcolor;
    void Start()
    {
        rend = objectToSpawn.GetComponent<Renderer>();
    }
    void Update()
    {
        increment += Time.deltaTime;
        transform.RotateAround(Vector3.zero, Vector3.up, 20 * Time.deltaTime);
        if (increment > limit )
        {
            increment = 0f;
            objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(1, 10);
            rend.sharedMaterial.SetColor("_Color", randomcolor);
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
            rb.AddForce(Vector3.right * 400, ForceMode.Impulse);
        }
    }
}
