﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trail : MonoBehaviour
{
    // With Range it possible to limit the values that variables can take
    [Range(1, 50)]
    public float startWidht = 5;
    [Range(1, 50)]
    public float endWidht = 2;

    //Update is called once per frame
    void Update()
    {
        //Accessing trailRenderer component present in the same GameObject
        GetComponent<TrailRenderer>().time = Random.Range(1, 100);
        GetComponent<TrailRenderer>().startWidth = startWidht;
        GetComponent<TrailRenderer>().endWidth = endWidht;
    }
}